var express = require('express'),
	app  = express(),
	server = require('http').createServer(app);

var io = require('socket.io')(server);

var Twitter = require('twitter');


server.listen(process.env.PORT || 80);

app.use(express.static( __dirname +'/public'));

app.get('/',function(req,res){
  res.sendfile( 'animation.html');
});


var client = new Twitter({
	consumer_key: process.env.TWITTER_CONSUMER_KEY,
	consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
	access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
	access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});


client.stream('statuses/filter', {track: 'cultivametro'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log(tweet.text);
    io.emit('tweet',{tweet:tweet.text});
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});

